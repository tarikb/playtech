# playTech

Il s'agit d'un site e-commerce proposant des produits high-tech.


## Fonctionnalités


### Dashboard admin


- La route côté front pour accéder au dashboard admin : admin/products
- Les routes sont protégées  : 
    - côté front avec la garde  
    -  côté back dans le SecurityConfig

- Seul un user ayant le rôle admin peut ajouter, modifier ou supprimer un produit.

### Recherche par nom du produit :
La barre de recherche située dans le header permet de filtrer la liste des produits par leur nom.
La requête de la méthode search dans le  product repository permet  d'effectuer une recherche sur les noms des produits.
Côté controller, elle est liée à la route search/{term}.

### Recherche par categorie :

L'onglet categories permet à l'utilisateur d'utiliser un menu déroulant pour selectionner une liste de produit selon sa catégorie.
Pour cette fonctionnalité, nous avons utilisé la méthode findByCategory proposée par JPA.


Le payback : Un utilisateur qui à acheté un produit, peut décider de le revendre. Le prix de revente dependra de l’état du produit ainsi que de sa date d’achat.

## User case

- L’invité :

- A accès aux produits
- Peut créer un compte
- Peut se connecter
-Accès au panier
-Acheter un produit

-L’utilisateur connecté : 

- A accès aux produits
- Peut créer un compte
- Peut se connecter
-Accès au panier
-Acheter un produit
- Accéder à son historique
- Acceder à des jeux concours
- (payback son produit)
- ajouter des commentaires sur des articles
- Pouvoir noter un article

- Admin :

- Ajouter des produits à la revente
- (Controller le payback)
- Créer les campagnes pour les jeux concours
- Modérer le contenu des utilisateurs et pouvoir supprimer des commentaires



