package co.simplon.p18.play_tech_back.controller;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.play_tech_back.entity.Score;
import co.simplon.p18.play_tech_back.repository.ScoreRepository;

@RestController
@RequestMapping("/api/score")
public class ScoreController {
    @Autowired
    private ScoreRepository scoreRepo;

    @GetMapping
    public List<Score> getAll() {
        return scoreRepo.findAll();
    }


    @GetMapping("/{id}")
    public Score getOne(@PathVariable int id) {
        Score score = scoreRepo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return score;
    }

    @PostMapping
    public Score add(@RequestBody @Valid Score score) {

        score.setId(null);
        return scoreRepo.save(score);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        Score toDelete = getOne(id); 
        scoreRepo.delete(toDelete);
    }

    @PutMapping("/{id}")
    public Score update(@PathVariable int id, @Valid @RequestBody Score score) {
        Score toUpdate = getOne(id);
        toUpdate.setContent(score.getContent());
        toUpdate.setRate(score.getRate());
        toUpdate.setCreation(score.getCreation());

        return scoreRepo.save(toUpdate);
    }
}
