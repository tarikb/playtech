package co.simplon.p18.play_tech_back.controller;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.play_tech_back.entity.LineProduct;
import co.simplon.p18.play_tech_back.entity.Product;
import co.simplon.p18.play_tech_back.entity.User;
import co.simplon.p18.play_tech_back.repository.LineProductRepository;
import co.simplon.p18.play_tech_back.repository.ProductRepository;
import co.simplon.p18.play_tech_back.repository.UserRepository;

@RestController
@RequestMapping("/api/lineproduct")

public class LineProductController {
    @Autowired
    private LineProductRepository lineProductRepo;
    @Autowired
    private ProductRepository productRepo;
    @Autowired
    private UserRepository userRepo;

    @GetMapping
    public List<LineProduct> getAll(@AuthenticationPrincipal User user) {
        //User currentUser=userRepo.findByEmail(user.getEmail()).get();
        List<LineProduct> lineProduct=lineProductRepo.findByUserId(user.getId());
        return lineProduct;
    }

    @GetMapping("/{id}")
    public LineProduct getOne(@PathVariable int id) {
        LineProduct lineProduct = lineProductRepo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return lineProduct;
    }

    @PostMapping("/{id}")
    public LineProduct add(@PathVariable int id, @AuthenticationPrincipal User user) {

        //lineProduct.setId(null);
        //lineProduct.setQuantity(1);
        //récupérer le product par son id avec le product repo
        Product newProduct = productRepo.findById(id).orElseThrow();
        
        //créer une instance de lineproduct
        LineProduct lineProduct = new LineProduct(1,newProduct.getPrice());
        lineProduct.setProduct(newProduct);
        lineProduct.setUserId(user.getId());
        //mettre le produit dedans, mettre une quantité par défaut, le prix du product

        //faire persister le tout

        // Product product = productService.getProductById(addToCartDto.getProductId());

        //à terme, mettre cette lineproduct dans le panier/order? du user connecté

        //  product = product.getId()
        return lineProductRepo.save(lineProduct);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        LineProduct toDelete = getOne(id); 
        lineProductRepo.delete(toDelete);
    }

    @PutMapping("/add")
    public LineProduct update(@PathVariable int id, @Valid @RequestBody LineProduct lineProduct) {
        LineProduct toUpdate = getOne(id);
        toUpdate.setPrice(lineProduct.getPrice());
        toUpdate.setQuantity(lineProduct.getQuantity());

        return lineProductRepo.save(toUpdate);
    }
}