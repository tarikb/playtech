package co.simplon.p18.play_tech_back.controller;
import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.play_tech_back.entity.Ordered;
import co.simplon.p18.play_tech_back.entity.User;
import co.simplon.p18.play_tech_back.repository.OrderedRepository;

@RestController
@RequestMapping("/api/order")
public class OrderedController {
    
    @Autowired
    private OrderedRepository orderRepo;

    @GetMapping
    public List<Ordered> getAll() {
        return orderRepo.findAll();
    }


    @GetMapping("/{id}")
    public Ordered getOne(@PathVariable int id) {
        Ordered order = orderRepo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return order;
    }

    @PostMapping
    public Ordered add(@RequestBody @Valid Ordered order, @AuthenticationPrincipal User user) {

        order.setId(null);
        order.setDate(LocalDate.now());
        order.setUser(user);
        return orderRepo.save(order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        Ordered toDelete = getOne(id); 
        orderRepo.delete(toDelete);
    }

    @PutMapping("/{id}")
    public Ordered update(@PathVariable int id, @Valid @RequestBody Ordered order) {
        Ordered toUpdate = getOne(id);
        toUpdate.setAddress(order.getAddress());
        toUpdate.setDate(order.getDate());
        toUpdate.setStatus(order.getStatus());

        return orderRepo.save(toUpdate);
    }
}
