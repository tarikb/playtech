package co.simplon.p18.play_tech_back.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.p18.play_tech_back.entity.Ordered;

public  interface OrderedRepository extends JpaRepository<Ordered, Integer>{
    
}
