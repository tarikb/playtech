package co.simplon.p18.play_tech_back.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.p18.play_tech_back.entity.Category;

public interface CategoryRepository extends JpaRepository<Category,Integer>{
    Optional<Category> findByName(String name);

}
