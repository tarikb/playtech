package co.simplon.p18.play_tech_back.auth;

public class ChangePasswordDto {
  public String oldPassword;
  public String newPassword;
}
