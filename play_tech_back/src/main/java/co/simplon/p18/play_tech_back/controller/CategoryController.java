package co.simplon.p18.play_tech_back.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.play_tech_back.entity.Category;
import co.simplon.p18.play_tech_back.repository.CategoryRepository;

@RestController
@RequestMapping("/api/category")

public class CategoryController {
    @Autowired
    private CategoryRepository categoryRepo;

    @GetMapping
    public List<Category> getAll() {
        return categoryRepo.findAll();
    }


    @GetMapping("/{id}")
    public Category getOne(@PathVariable int id) {
        Category category = categoryRepo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return category;
    }

    @PostMapping
    public Category add(@RequestBody @Valid Category category, String email) {
        if(categoryRepo.findByName(category.getName()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Category already exists");
        }
        category.setId(null);
        return categoryRepo.save(category);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        Category toDelete = getOne(id); 
        categoryRepo.delete(toDelete);
    }

    @PutMapping("/{id}")
    public Category update(@PathVariable int id, @Valid @RequestBody Category category) {
        Category toUpdate = getOne(id);
        toUpdate.setName(category.getName());

        return categoryRepo.save(toUpdate);
    }
}
