package co.simplon.p18.play_tech_back.auth;

public enum UserRoles {
  ROLE_USER,
  ROLE_ADMIN
}
