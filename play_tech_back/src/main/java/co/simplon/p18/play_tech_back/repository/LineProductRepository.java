package co.simplon.p18.play_tech_back.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.p18.play_tech_back.entity.LineProduct;

public interface LineProductRepository extends JpaRepository<LineProduct,Integer>{
    List<LineProduct> findByUserId(Integer userId);
}
