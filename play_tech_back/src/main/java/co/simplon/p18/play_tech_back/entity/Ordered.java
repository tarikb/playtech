package co.simplon.p18.play_tech_back.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Ordered {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String address;
    private String status;
    private LocalDate date;
    private double totalAmount;

    @ManyToOne
    @JsonIgnore
    private User user;

    @JsonIgnore
    @OneToMany
    private List<LineProduct> lineProducts = new ArrayList<>();
    
    public Ordered(String address, String status, LocalDate date, double totalAmount, User user,
        List<LineProduct> lineProducts) {
      this.address = address;
      this.status = status;
      this.date = date;
      this.totalAmount = totalAmount;
      this.user = user;
      this.lineProducts = lineProducts;
    }

    public Ordered(Integer id, String address, String status, LocalDate date, double totalAmount,
        User user, List<LineProduct> lineProducts) {
      this.id = id;
      this.address = address;
      this.status = status;
      this.date = date;
      this.totalAmount = totalAmount;
      this.user = user;
      this.lineProducts = lineProducts;
    }

    public Ordered() {
    }
  
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public List<LineProduct> getLineProducts() {
        return lineProducts;
    }
    public void setLineProducts(List<LineProduct> lineProducts) {
        this.lineProducts = lineProducts;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public double getTotalAmount() {
      return totalAmount;
    }
    public void setTotalAmount(double totalAmount) {
      this.totalAmount = totalAmount;
    }

}
