package co.simplon.p18.play_tech_back.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.simplon.p18.play_tech_back.entity.Category;
import co.simplon.p18.play_tech_back.entity.Product;

@Repository
public  interface ProductRepository extends JpaRepository<Product,Integer>{
    Optional<Product> findByName(String name);
    // Optional<Product>  findByCategories(Set<Category> categories);;
    Optional<Product> findById(Integer id);

   @Query("SELECT p FROM Product p  WHERE p.name LIKE %:term%")
    List<Product> search(String term);

    List<Product> findByCategory(Category category);
}
