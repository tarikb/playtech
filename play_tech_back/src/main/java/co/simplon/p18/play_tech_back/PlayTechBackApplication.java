package co.simplon.p18.play_tech_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlayTechBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlayTechBackApplication.class, args);
	}

}
