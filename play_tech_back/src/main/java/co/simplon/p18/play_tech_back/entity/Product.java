package co.simplon.p18.play_tech_back.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private String picture;
    private String status;
    private Integer stock;
    private Integer price;

    @OneToMany
    @JsonIgnore
    private List<LineProduct> lineProducts = new ArrayList<>();

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Set<Score> scores = new HashSet<>();

    public Product() {
    }

    public Product(String name, String description, String picture, String status, Integer stock, Integer price) {
        this.name = name;
        this.description = description;
        this.picture = picture;
        this.status = status;
        this.stock = stock;
        this.price = price;
    }

    public Product(Integer id, String name, String description, String picture, String status, Integer stock,
            Integer price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.picture = picture;
        this.status = status;
        this.stock = stock;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    // public List<LineProduct> getLineProducts() {
    // return lineProducts;
    // }
    // public void setLineProducts(List<LineProduct> lineProducts) {
    // this.lineProducts = lineProducts;
    // }
    public Set<Score> getScores() {
        return scores;
    }

    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }

}
