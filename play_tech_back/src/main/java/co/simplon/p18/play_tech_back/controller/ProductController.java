package co.simplon.p18.play_tech_back.controller;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.play_tech_back.entity.Category;
import co.simplon.p18.play_tech_back.entity.Product;
import co.simplon.p18.play_tech_back.repository.CategoryRepository;
import co.simplon.p18.play_tech_back.repository.ProductRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    private ProductRepository productRepo;
    private CategoryRepository categoryRepo;

    // @GetMapping
    // public List<Product> getAll() {

    //     return productRepo.findAll();
    // }

    /**
     * Méthode qui permet de récupérer une liste de chien paginée à laquelle on donne
     * la page à afficher et le nombre d'élément par page
     * Accessible via http://localhost:8080/api/dog?page=2&pageSize=15 par exemple (mais la page et pageSize sont optionnels)
     * @param page Page à afficher, 0 par défaut
     * @param pageSize Nombre d'élément par page, 10 par défaut
     * @return Une Page de chien contenant non seulement les chiens, mais aussi toutes les informations de pagination (total de page, nombre d'élément total etc.)
     */
    @GetMapping
    public Page<Product> getAll(
    @RequestParam(required = false, defaultValue = "0") int page,
    @RequestParam(required = false, defaultValue = "10") int pageSize) {
        return productRepo.findAll(PageRequest.of(page, pageSize));
    }

   @GetMapping("/search/{term}")
    public List<Product> search(@PathVariable String term){
      return productRepo.search(term);
    }
    
    @GetMapping("/{id}")
    public Product getOne(@PathVariable int id) {
        Product product = productRepo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return product;
    }
    @GetMapping("/category/{id}")
    public List<Product> getByCategory(@PathVariable int id){
        Category category= new Category(id,"");
        return productRepo.findByCategory(category);

    }


    @PostMapping
    public Product add(@RequestBody @Valid Product product, String email) {
        if(productRepo.findByName(product.getName()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"product already exists");
        }
        product.setId(null);
        return productRepo.save(product);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        Product toDelete = getOne(id); 
        productRepo.delete(toDelete);
    }

    @PutMapping("/{id}")
    public Product update(@PathVariable int id, @Valid @RequestBody Product product) {
        Product toUpdate = getOne(id);
        toUpdate.setName(product.getName());
        toUpdate.setDescription(product.getDescription());
        toUpdate.setPicture(product.getPicture());
        toUpdate.setPrice(product.getPrice());
        toUpdate.setStatus(product.getStatus());
        toUpdate.setStock(product.getStock());


        return productRepo.save(toUpdate);
    }

}
