package co.simplon.p18.play_tech_back.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDate creation;
    private String content;
    private Integer rate;

    @ManyToOne
    @JsonIgnoreProperties("scores")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("scores")
    private Product product;

    public Score() {
    }

    public Score(LocalDate creation, String content, Integer rate) {
        this.creation = creation;
        this.content = content;
        this.rate = rate;
    }

    public Score(Integer id, LocalDate creation, String content, Integer rate) {
        this.id = id;
        this.creation = creation;
        this.content = content;
        this.rate = rate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getCreation() {
        return creation;
    }

    public void setCreation(LocalDate creation) {
        this.creation = creation;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
