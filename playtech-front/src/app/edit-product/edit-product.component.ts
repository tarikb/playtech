import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  product:Product={id:1,name:"",description:"",picture:"",status:"",stock:0,price:0,category:{id:1,name:''}};
routeId?:string;
categories?:Category[];
  constructor(private route:ActivatedRoute, private pS:ProductService, private cS:CategoryService, private router:Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(param=>{this.routeId=param['id'];})
    this.pS.getOne(Number(this.routeId)).subscribe(data=>this.product=data)
    this.cS.getAll().subscribe(data=>this.categories=data);

  }
  onSubmit(){
    console.log(this.product);
    this.pS.update(this.product).subscribe((data)=>{
      this.router.navigateByUrl("/")});
  
  }
}
