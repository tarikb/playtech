import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { LineProduct, Ordered, Product } from '../entities';
import { OrderedService } from '../ordered.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  LinesProduct?: LineProduct[] = [];
  product?: Product;
  ordered:Ordered={
    id:0,
    status:'',
    totalAmount:0
  }
  // id?:number
  //   address:string;
  //   status:string;
  //   date?:Date;
  //   totalAmount:number;



  constructor(private cartService: CartService, private orderedService: OrderedService) { }

  ngOnInit(): void {
    this.fetchLinesProduct()
  }
  fetchLinesProduct() {
    this.cartService.getLinesProduct().subscribe(data => this.LinesProduct = data);
  }
  saveOrder(){
    let amount:number=0;
    for (const item of this.LinesProduct!) {
      amount+=item.price*item.quantity;
    }
    console.log(amount);
    this.ordered.totalAmount=amount;
    this.ordered.status='validated';
    this.orderedService.save(this.ordered).subscribe();
  }

  deleteLineProduct(id: number) {
    this.cartService.delete(id).subscribe();
  }


}
