import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string = '';
  password:string = '';
  hasError = false;


  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }
  login(){
    this.hasError = false;
    this.auth.login(this.email,this.password).subscribe({
      next: data => this.router.navigate(['/']),
      error: () => this.hasError = true
    });

    
  }
}
