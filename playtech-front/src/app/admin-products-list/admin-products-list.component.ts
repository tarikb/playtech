import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category, Page, Product, Score } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-admin-products-list',
  templateUrl: './admin-products-list.component.html',
  styleUrls: ['./admin-products-list.component.css']
})
export class AdminProductsListComponent implements OnInit {
  products?: Page<Product>;
  scores?: Score[] = [];
  categories: Category[] = [];
  page = 0;
  pageSize = 10;
  constructor(private productService: ProductService, private router:Router) { }

  ngOnInit(): void {
    this.fetchProducts();

  }

  delete(id:number){
    this.productService.delete(id).subscribe(()=>this.router.navigateByUrl("/admin/products"))
  }
  nextPage() {
    this.page++;
    this.fetchProducts();
  }
  previousPage() {
    this.page--;
    this.fetchProducts();
  }

  fetchProducts() {
    this.productService.getAll(this.page, this.pageSize).subscribe(data => this.products = data);
  }
}
