import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultByCategoryComponent } from './result-by-category.component';

describe('ResultByCategoryComponent', () => {
  let component: ResultByCategoryComponent;
  let fixture: ComponentFixture<ResultByCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultByCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultByCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
