import { Component, Input, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Category, Page, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-result-by-category',
  templateUrl: './result-by-category.component.html',
  styleUrls: ['./result-by-category.component.css']
})
export class ResultByCategoryComponent implements OnInit {
@Input()
products?:Product[];
product?: Page<Product>;
categories: Category[] = [];
page = 0;
pageSize = 10;
  constructor(private productService: ProductService, private cartService: CartService) { }

  ngOnInit(): void {
  }

  nextPage() {
    this.page++;
    this.fetchProducts();
  }
  previousPage() {
    this.page--;
    this.fetchProducts();
  }

  fetchProducts() {
    this.productService.getAll(this.page, this.pageSize).subscribe(data => this.product = data);
  }

  addToCart(id:number) { 
    this.cartService.add(id).subscribe();
    console.log(id);
    
  }
}
