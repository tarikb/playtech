import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  label='';
  constructor(private router:Router) { }

  ngOnInit(): void {
  }
search(){
  this.router.navigateByUrl('search/'+this.label);
}
}
