import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category, Page, Product } from './entities';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }
  // getAll() {
  //   return this.http.get<Product[]>('http://localhost:8080/api/product');
  // }

  getAll(page = 0, pageSize = 10) {
    //Fera une requête vers http://localhost:8080/api/dog?page=0&pageSize=10 par défaut
    return this.http.get<Page<Product>>('/api/product', {
      params:{page,pageSize}
    });
  }
  search(term:string){
    return this.http.get<Product[]>('/api/product/search/'+term);
  }
  getOne(id:number) {
    return this.http.get<Product>('/api/product/'+id);
  }

  getByCategory(category:Category){
    return this.http.get<Product[]>('/api/product/category/'+category.id);
  }
  add(product:Product){
    return this.http.post<Product>('/api/product',product)
  }
  delete(id:number){
    return this.http.delete<number>('/api/product/'+id)
  }

  update(product:Product){
    return this.http.put<Product>('/api/product/'+product.id,product)
  }
}
