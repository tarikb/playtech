import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Category, LineProduct, Page, Product, Score } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products?: Page<Product>;
  scores?: Score[] = [];
  categories: Category[] = [];
  page = 0;
  pageSize = 10;
  LineProduct?: LineProduct;
  constructor(private productService: ProductService, private cartService: CartService) { }

  ngOnInit(): void {
    this.fetchProducts();

  }


  nextPage() {
    this.page++;
    this.fetchProducts();
  }
  previousPage() {
    this.page--;
    this.fetchProducts();
  }

  fetchProducts() {
    this.productService.getAll(this.page, this.pageSize).subscribe(data => this.products = data);
  }

  addToCart(id:number) { 
    this.cartService.add(id).subscribe();
    console.log(id);
    
  }
}