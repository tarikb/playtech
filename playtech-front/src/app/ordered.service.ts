import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Ordered } from './entities';

@Injectable({
  providedIn: 'root'
})
export class OrderedService {

  constructor(private http: HttpClient) { }

  save(ordered: Ordered){
    return this.http.post('/api/order', ordered);
  }
}
