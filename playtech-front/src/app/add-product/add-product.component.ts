import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../category.service';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  product:Product={id:1,name:"",description:"",picture:"",status:"",stock:0,price:0,category:{id:1,name:''}};
  routeId?:string;
  categories?:Category[];
    constructor(private pS:ProductService, private cS:CategoryService, private router:Router) { }
  
    ngOnInit(): void {
      
      this.cS.getAll().subscribe(data=>this.categories=data);
      
    }
    onSubmit(){
      this.pS.add(this.product).subscribe(()=>this.router.navigateByUrl('/'));

    }
}
