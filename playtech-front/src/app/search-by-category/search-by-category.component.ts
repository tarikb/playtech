import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-search-by-category',
  templateUrl: './search-by-category.component.html',
  styleUrls: ['./search-by-category.component.css']
})
export class SearchByCategoryComponent implements OnInit {
  categories:Category[]=[];
  products:Product[]=[];
  selectedCategory?:Category;
  constructor(private productService:ProductService, private cS:CategoryService) { }
  onSubmit(){
    if(this.selectedCategory){
      this.productService.getByCategory(this.selectedCategory).subscribe(data => this.products=data);
      console.log(this.products);
    }
 
  }
  ngOnInit(): void {
    this.cS.getAll().subscribe(data=>this.categories=data);
  }

}
