import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddProductComponent } from './add-product/add-product.component';
import { AdminProductsListComponent } from './admin-products-list/admin-products-list.component';
import { AdminGuard } from './admin.guard';
import { CartComponent } from './cart/cart.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResultSearchProductComponent } from './result-search-product/result-search-product.component';
import { SearchByCategoryComponent } from './search-by-category/search-by-category.component';
import { SingleProductComponent } from './single-product/single-product.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'product/:id', component: SingleProductComponent},
  {path:'cart', component: CartComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'search/:term',component:ResultSearchProductComponent},
  {path:'category',component:SearchByCategoryComponent},
  {path:'admin/products',component:AdminProductsListComponent,canActivate:[AdminGuard]},
  {path:'admin/products/edit/:id',component:EditProductComponent, canActivate:[AdminGuard]},
  {path:'admin/products/add',component:AddProductComponent,canActivate:[AdminGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
