import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LineProduct, Product } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private http:HttpClient) { }

  getLinesProduct() {
    return this.http.get<LineProduct[]>('/api/lineproduct');
  }

  delete(id:number) {
    return this.http.delete('/api/lineproduct/'+id);
  }

  add(id:number) {
    return this.http.post<LineProduct>('/api/lineproduct/'+id,null);
  }
}
