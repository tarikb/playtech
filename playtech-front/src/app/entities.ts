import { ɵExtraLocaleDataIndex } from "@angular/core";

export interface Product {
    id:number;
    name:string;
    description:string;
    picture:string;
    status:string;
    stock:number;
    price:number;
    score?:Score[];
    category:Category;

}

export interface Score {
    id:number;
    content:string;
    rate:number;
}

export interface Category {
    id:number;
    name:string
}

  /**
   * Ici on fait une interface Page générique qui reprend
   * les propriétés des Page<T> en Java ce qui nous permettra
   * de récupérer les informations de paginations
   * (Le générique signifie que cette interface pourra servir pour 
   * n'importe quel autre type, exactement comme on peut faire une List<Dog> 
   * ou une List<Person> en java, ici on pourra faire un Page<Dog> ou un Page<Person>)
   */
   export interface Page<T> {
    content: T[];
   
    totalPages: number;
    totalElements: number;
    last: boolean;
    first: boolean;

    size: number;
    number: number;
    numberOfElements: number;
    empty: boolean;
}

export interface User {
  id?:number;
  name?:string;
  firstname?:string;
  email:string;
  password?:string;
  phoneNumber?:string;
  address?:string;
  zipCode?:string;
  ville?:string;
  birthDate?:Date;
  createdAccount?:Date;
  role?:string;

}
export interface Ordered {
    id?:number
    address?:string;
    status:string;
    date?:Date;
    totalAmount:number;

}

export interface LineProduct {
    id:number;
    quantity:number;
    price:number;
    product:Product;
}
