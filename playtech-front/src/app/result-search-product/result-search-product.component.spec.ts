import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultSearchProductComponent } from './result-search-product.component';

describe('ResultSearchProductComponent', () => {
  let component: ResultSearchProductComponent;
  let fixture: ComponentFixture<ResultSearchProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultSearchProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultSearchProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
