import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-result-search-product',
  templateUrl: './result-search-product.component.html',
  styleUrls: ['./result-search-product.component.css']
})
export class ResultSearchProductComponent implements OnInit {
  products?:Product[];
  term?:string;
  constructor(private productService:ProductService, private route:ActivatedRoute) { }

  ngOnInit(): void {
   this.route.params.subscribe(param=>{this.term=param['term'];
   this.productService.search(this.term!).subscribe(data=>this.products=data);});

   console.log(this.term)
   
   console.log(this.products)
  }
/*search(test:string){
  this.route.params.subscribe(para)
  return this.productService.search(test).subscribe(data=>this.products=data);
}*/
}
