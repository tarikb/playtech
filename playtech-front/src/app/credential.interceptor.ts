import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class CredentialInterceptor implements HttpInterceptor {

  constructor(private router:Router, private auth:AuthService) { }
  
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request = request.clone({
      setHeaders: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      withCredentials: true,
      url: environment.apiUrl + request.url
    });


    return next.handle(request).pipe(
      tap({
        error: (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status != 401) {
              return;
            }
            this.auth.logout().subscribe();
            this.router.navigate(['login']);
          }
        }
      }))
  }

}
