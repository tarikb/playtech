# Sprint 3

## Difficultés rencontrées 
- Prioriser les tâches
- Estimation du temps nécessaire pour réaliser certaines tâches (panier)
- 


## Améliorations possibles
- Découper la tâche en plusieurs petites.
- 

## Améliorations apportées
- Se référer au brief du projet pour définir les tâches prioritaires (fonctionnalité obligatoire)
- Amélioration de la communication
